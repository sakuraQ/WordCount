#include <stdio.h>
#include<string.h>
/*
int main(int argc, char* argv[])
argc是命令行总的参数个数，由编译器自动计算。
argv[]是参数，其中argv[0]是程序的全名，后面的参数是命令行后面跟的用户输入的参数
*/
int main(int argc, char* argv[])
{
    int counts = 0;
    char ch;
    //读取文件
    FILE* fp = fopen(argv[2], "r");
    if (fp == NULL)
    {
        printf("错误：无法打开%s\n",argv[2]);
        return 0;
    }
    //匹配参数，"-w"统计单词数目，"-c"统计字符数目
    if (!strcmp(argv[1],"-w")) 
	{
        while ((ch = fgetc(fp)) != EOF) //在while循环中，从流中读取字符,以EOF作为文件结束标志
		{
            if (ch == ' ' || ch == ',') 
			{
                counts++;
            }
        }
        printf("单词数：%d\n", counts+1);
    }
    else if (!strcmp(argv[1],"-c")) 
	{
        while ((ch = fgetc(fp)) != EOF) 
		{
            counts++;
        }
        printf("字符数：%d\n", counts);
    }
    else 
	{
        printf("参数错误 ！");
    }
    fclose(fp);
    return 0;
}
